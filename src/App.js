import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js';
import Footer from './components/Footer.js'

import CartCheckout from './pages/CartCheckout.js';
import Error from './pages/Error.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import ServiceDetail from './pages/ServiceDetail.js';
import Services from './pages/Services.js';
import UserDashboard from './pages/UserDashboard.js';

import './App.css';

import {UserProvider} from './UserContext.js';


function App() {

  const [user, setUser] = useState({id : null, email : null, isAdmin : null});

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    if(localStorage.getItem("token")){
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers : {
          Authorization : `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {
        setUser({
        id : data._id,
        isAdmin : data.isAdmin,
        email : data.email
        })
      })
    }
  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container className="bground min-vh-100" fluid>
            <Routes>
              <Route path="/*" element={<Error />} />
              <Route path="/" element={<Home />} />
              <Route path="/checkout" element={<CartCheckout />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/register" element={<Register />} />
              <Route path="/services" element={<Services />} />
              <Route path="/services/:serviceId" element={<ServiceDetail />} />
              <Route path="/userDashboard" element={<UserDashboard />} />
            </Routes>
          </Container>
          <Footer />
      </Router>
    </UserProvider>
  );
}

export default App;