import {Row, Col, Card, Carousel, Image, Container} from 'react-bootstrap';



export default function Highlights() {
  return (
  	<Container className="pt-3 pb-3" fluid>
	    <Carousel controls={false} fade>
	      <Carousel.Item>
	        <img
	          className="d-block w-100 carousel-img"
	          src={require("./images/circuits.jpg")}
	          alt="circuits"
	        />
	        <Carousel.Caption className="carousel-text">
	          <h3>CIRCUITS</h3>
	          <p>Basic Circuit Design Services for Academic Projects</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100 carousel-img"
	          src={require("./images/programming.jpg")}
	          alt="programming"
	        />

	        <Carousel.Caption className="carousel-text">
	          <h3>PROGRAMMING</h3>
	          <p>Web Development Services, App Development Services</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100 carousel-img"
	          src={require("./images/prototyping.png")}
	          alt="prototyping"
	        />

	        <Carousel.Caption className="carousel-text">
	          <h3>PROTOTYPING</h3>
	          <p>Prototype Project Services</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	    </Carousel>
    </Container>
  );
}