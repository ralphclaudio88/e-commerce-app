import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}) {
	
    const {title, content, destination, label} = data;

    return (
        <Row className="text-center">
            <Col className="p-3">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-primary btn-block" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}