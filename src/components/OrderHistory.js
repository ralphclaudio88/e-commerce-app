import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Form, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function OrderHistory({order}) {

	const { _id, totalAmount, isPaid, purchasedOn, owner, isValid } = order;

	const { user } = useContext(UserContext);

	const [isCancelled, setIsCancelled] = useState(isValid);
	const [isActivated, setIsActivated] = useState(!isValid);
	const [paid, setPaid] = useState(isPaid);

	const [newQuantity, setNewQuantity] = useState(true);

	function cancelOrder(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/orders/deleteOrder/${_id}`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Order Cancelled!',
        		  text: 'You have cancelled this order.'
        		})
            	setIsCancelled(isActivated)
            	setIsActivated(!isActivated)
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in cancelling!',
        		  text: 'Order is either already cancelled or paid.'
        		})
            	setIsCancelled(isActivated)
        		setIsActivated(!isActivated)
        	}
        })
	}

	function reAddOrder(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/orders/reAddOrder/${_id}`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Order Re-Added!',
        		  text: 'You have re-added this order to cart.'
        		})
        		setIsActivated(!isActivated)
        		setIsCancelled(isActivated)
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in Re-Adding!',
        		  text: 'Order is either already added or paid.'
        		})
        	}
        })
	}

	useEffect(()=>{
		if(isCancelled){
			setIsActivated(false)
		}
	})
	
	return order.products.map((orderContent, i) => {

		const [newTotal, setNewTotal] = useState(totalAmount);

		return (
			<Container className="p-3" key={i}>
				<Card className="col-12 order-card text-light">
		            <Card.Body className="bg-secondary">
		                <Card.Title>{`OrderId: ${_id}`}</Card.Title>
		                <Card.Title>{`Order Owner: ${owner}`}</Card.Title>
		                <Card.Title as="h2">{`${orderContent.productName}`}</Card.Title>
		                <Card.Text className="m-0">{`Quantity: ${orderContent.quantity}`}</Card.Text>
		                
		                <Card.Text>{`Unit Price: Php ${orderContent.unitPrice}`}</Card.Text>
		                <Card.Text>{`Total: Php ${newTotal}`}</Card.Text>
		                <Card.Text>{`Paid: ${isPaid}`}</Card.Text>
		                <Card.Text>{`Validity: ${isCancelled}`}</Card.Text>
		                <Card.Subtitle>{`PurchasedOn: ${purchasedOn}`}</Card.Subtitle>
		                {
		                user.isAdmin ?
		                <></>
		                :
		                <>
		                {
		                isPaid ?
		                <></>
		                :
		                isActivated ?
		                <Link className="btn btn-success mt-2" onClick={(e) => {
		                	reAddOrder(e)
		                }}>Activate</Link>
		                :
		                <Link className="btn btn-danger mt-2" onClick={(e) => {
		                	cancelOrder(e)
		                }}>Cancel</Link>
		                }
		                </>
		            	}
		            </Card.Body>
		        </Card>
			</Container>
    )
	})
}