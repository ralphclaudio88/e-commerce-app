import { useState, useEffect } from 'react';
import { Card, Button, Container, Col, Row, Form } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function AllProducts({product}) {

	const { _id, productName, description, price, isActive } = product;

	const [id, setId] = useState(_id);
	const [name, setName] = useState(productName);
	const [desc, setDesc] = useState(description);
	const [pri, setPri] = useState(price);
	const [isAvailable, setIsAvailable] = useState(isActive);


	const [isEdit, setIsEdit] = useState(true);
	const [isSave, setIsSave] = useState(false);
	const [isEditName, setIsEditName] = useState(false);
	const [isArchived, setIsArchived] = useState(false);

	const [newName, setNewName] = useState('');
	const [newDescription, setNewDescription] = useState('');
	const [newPrice, setNewPrice] = useState('');


	function updateProductName(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/updateProduct`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	},
        	body : JSON.stringify({
        		productName : newName
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		console.log(data);
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Service Offer Updated!',
        		  text: 'You have successfully updated Service information.'
        		})
        		setNewName('');
        		setId(_id)
        		setName(newName);
        		setNewDescription('');
        		setNewPrice('');
        		setIsEdit(true);
        	} else {
        		console.log(data);

        		Swal.fire({
        		  icon: 'error',
        		  title: 'Duplicate service name found!',
        		  text: 'Please check if you are updating the same service name or if service name has duplicate.'
        		})
        	}
        })
	}

	function updateProduct(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/updateProduct`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	},
        	body : JSON.stringify({
    		    description : newDescription,
    		    price : newPrice
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		console.log(data);
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Service Offer Updated!',
        		  text: 'You have successfully updated Service information.'
        		})
        		setNewName('');
        		setId(_id)
        		setDesc(newDescription);
        		setPri(newPrice);
        		setNewDescription('');
        		setNewPrice('');
        		setIsEdit(true);
        	} else {
        		console.log(data);

        		Swal.fire({
        		  icon: 'error',
        		  title: 'Duplicate service offer found!',
        		  text: 'Please check if you are updating into the same service offer.'
        		})
        	}
        })
	}

	function archiveProduct(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		console.log(data);
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Service Offer Archived!',
        		  text: 'You have successfully archived this Service.'
        		})
        		setIsEdit(true);
        	} else {
        		console.log(data);

        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in archiving product!',
        		  text: 'Product is already archived.'
        		})
        	}
        })
	}

	function activateProduct(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		console.log(data);
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Service Offer Activated!',
        		  text: 'You have successfully activated this Service. It will be visible to visitors and clients'
        		})
        		setIsEdit(true);
        	} else {
        		console.log(data);

        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in activating product!',
        		  text: 'Product is already activated.'
        		})
        	}
        })
	}

	useEffect(() => {
		if((newName !== "") || (newDescription !== "") && (newPrice !== "" && !isNaN(newPrice))) {
			setIsSave(true);
		} else {
			setIsSave(false);
		}
	}, [newName, newDescription, newPrice])

	return (
		isEdit ?
		<Container className="p-3 product-card">
			<Card className="col-12 col-md-12">
	            <Card.Body>
	                <Card.Title>{`Product ID: ${id}`}</Card.Title>
	                <Card.Title as="h2">{`${name}`}</Card.Title>
	                <Card.Title>{`Description:`}</Card.Title>
	                <Card.Text as="h6">{`${desc}`}</Card.Text>
	                <Card.Title>{`Php ${pri}.00`}</Card.Title>
	                <Card.Subtitle>{`Availability: ${isAvailable}`}</Card.Subtitle>
	                <Link className="btn btn-primary mt-2" onClick={() => setIsEdit(false)}>EDIT</Link>
	                {
	                (isAvailable) ?
    	            <Button variant="danger" className="mt-2" onClick={(e) => {
    	            	setIsAvailable(!isAvailable)
    	            	archiveProduct(e)
    	            }}>Archive</Button>
    	            :
    	            <Button variant="success" className="mt-2" onClick={(e) => {
    	            	setIsAvailable(!isAvailable)
    	            	activateProduct(e)
    	            }}>Activate</Button>
    	            }
	            </Card.Body>
	        </Card>
		</Container>
		:
		<Container className="p-3 product-card">
			{
			(isEditName) ?
			<Form className="form-color p-5" onSubmit={(e) => {updateProductName(e)
				setIsEdit(true)
		}}>
    			<h1 className="text-center" key={1}>Update Service Details</h1>
				<Form.Group controlId="service-name">
					<Form.Label className="mt-2 mb-0 text-light">Service Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter service name"
						value={newName}
						onChange={e=>setNewName(e.target.value)}
						required
					/>
					<Link className="btn btn-dark mt-2" onClick={() => {
						setNewName('');
						setIsEditName(false)
					}}>Cancel</Link>
				</Form.Group>
				<Form.Group controlId="service-description">
					<Form.Label className="mt-2 mb-0 text-light">Description</Form.Label>
					<Form.Label className="mt-2 mb-0 text-light d-block">{`${desc}`}</Form.Label>
				</Form.Group>

				<Form.Group controlId="service-price">
					<Form.Label className="mt-2 mb-0 text-light">Price</Form.Label>
					<Form.Label className="mt-2 mb-0 text-light d-block">{`${pri}`}</Form.Label>
				</Form.Group>
				{
					(isSave) ?
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">UPDATE</Button>
					:
					<Button variant="secondary" type="submit" id="submitBtn" className="mt-2" onClick={() => setIsEdit(true)}>Cancel</Button>
				}
			</Form>
			:
			<Form className="form-color p-5" onSubmit={(e) => updateProduct(e)}>
    			<h1 className="text-center" key={1}>Update Service Details</h1>
				<Form.Group controlId="service-name">
					<Form.Label className="mt-2 mb-0 text-light">Service Name</Form.Label>
					<Form.Label as="h2" className="mt-2 mb-0 text-light d-block">{`${name}`}</Form.Label>
					<Link className="btn btn-dark mt-2" onClick={() => setIsEditName(true)}>Edit</Link>
				</Form.Group>
				<Form.Group controlId="service-description">
					<Form.Label className="mt-2 mb-0 text-light">Description</Form.Label>
					<Form.Control
						type="textarea"
						placeholder="Enter description"
						value={newDescription}
						onChange={e=>setNewDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="service-price">
					<Form.Label className="mt-2 mb-0 text-light">Price</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter price in Php"
						value={newPrice}
						onChange={e=> setNewPrice(e.target.value)}
						required
					/>
				</Form.Group>
				{
					(isSave) ?
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">UPDATE</Button>
					:
					<Button variant="secondary" type="submit" id="submitBtn" className="mt-2" onClick={() => setIsEdit(true)}>Cancel</Button>
				}
			</Form>
			}
		</Container>
    )
}