import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Row, Table, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Cart from '../components/Cart.js';
import OrderHistory from '../components/OrderHistory.js';
import AllProducts from '../components/AllProducts.js';
import AllUsers from '../components/AllUsers.js';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function UserDetails({user}) {

	const { firstName, lastName, mobileNumber, isAdmin, coupon, email, password } = user;

	let [total, setTotal] = useState(0);

	const [orders, setOrders] = useState();
	const [users, setUsers] = useState();
	const [thisCart, setThisCart] = useState();
	const [products, setProducts] = useState();

	const [isList, setIsList] = useState(false);
	const [areAllUsers, setAreAllUsers] = useState(false);
	const [isCreated, setIsCreated] = useState(false);
	const [isAllService, setIsAllService] = useState(false);
	const [isCreateButton, setIsCreateButton] = useState(false);
	const [cart, setCart] = useState(false);
	const [isCheckout, setIsCheckout] =useState(false);

	const [refresh, setRefresh] = useState(false);

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	function addProduct(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/newProduct`,{
        	method : "POST",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	},
        	body : JSON.stringify({
        		productName : productName,
    		    description : description,
    		    price : price
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'New Service Offer Added!',
        		  text: 'You have successfully added a new Service to offer.'
        		})
				setProducts(products);
        		setProductName('');
        		setDescription('');
        		setPrice('');

        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Duplicate service offer found!',
        		  text: 'Please check if you are adding the same service offer.'
        		})
        	}
        })
	}

	function allProducts(e) {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers : {
    			Authorization : `Bearer ${localStorage.getItem('token')}`
    		}
		})
		.then(res => res.json())
		.then(allService => {
			setProducts(allService.map(product => {
				return (
					<AllProducts key={product._id} product={product} />
				)
			}))
		})
	}

	function myOrders(e) {
		fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`,{
				headers : {
	    			Authorization : `Bearer ${localStorage.getItem('token')}`
	    		}
			})
			.then(res => res.json())
			.then(orderData => {
				// setCart(false);
				setOrders(orderData.map(orderProduct => {
					return (
						<OrderHistory key={orderProduct._id} order={orderProduct} />
					)
				}))
			})
	}

	function allOrders(e) {
		fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`,{
				headers : {
	    			Authorization : `Bearer ${localStorage.getItem('token')}`
	    		}
			})
			.then(res => res.json())
			.then(allOrderData => {
				setOrders(allOrderData.map(allOrderProduct => {
					return (
						<OrderHistory key={allOrderProduct._id} order={allOrderProduct} />
					)
				}))
			})
	}

	function myCart(e) {
		fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,{
        	headers : {
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(cartData => {
        	// setCart(false);
        	setThisCart(cartData.map(cartItem => {
				return (
					<Cart key={cartItem._id} cartItems={cartItem} />
				)
			}))
		})
	}

	function allUsers(e){
		fetch(`${process.env.REACT_APP_API_URL}/users/all`,{
				headers : {
	    			Authorization : `Bearer ${localStorage.getItem('token')}`
	    		}
			})
			.then(res => res.json())
			.then(data => {
				setUsers(data.map(eachUser => {
					return (
						<AllUsers key={eachUser._id} userAll={eachUser} />
					)
				}))
			})
	}

	function calculateTotal(e){

		let finalTotal = 0;

		fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,{
        	headers : {
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(cartData => {
        	let totalArr = cartData.map(cartItem => {
				return cartItem.totalAmount + total;
			})
			if(totalArr.length !== 0){
				finalTotal = totalArr.reduce((a,b)=>a+b)
				let initializeTotal = finalTotal;
				return setTotal(initializeTotal);
			}
			
		})
	}

	function confirmation(e){
		if(e){
			Swal.fire({
			  icon: 'success',
			  title: 'Changes Applied!',
			  text: 'Click cart again to see changes and to checkout.'
			})
			setRefresh(false)
		}
	}

	useEffect((e)=> {
		if(user.isAdmin===false){
			myCart(e);
			myOrders(e);
			calculateTotal();
		};
		if(cart===true && user.isAdmin===false){
			setTotal(0)
			total=0
			calculateTotal(e)
		};
		if(isList===true && user.isAdmin===false){
			myOrders()
			myCart()
		}
	}, [cart, isList])

	useEffect(() => {
		if((productName !== "" && description !== "" && price !== "") && (!isNaN(price))) {
			setIsCreateButton(true);
		} else {
			setIsCreateButton(false);
		}
	}, [productName, description, price]);

	useEffect(() => {
		if(isCreated===false){
			setProductName('');
			setDescription('');
			setPrice('');
		}
	}, [isCreated])

	useEffect((e)=>{
		if(cart){
			myCart(e);
			myOrders(e);
		}
	}, [cart])

	useEffect(()=>{
		if(refresh===true){
			confirmation(refresh)
		}
	}, [refresh])

	useEffect((e)=>{
		if(user.isAdmin){
			allOrders()
		}
	}, [])



	return (
		<Row>
			<Container className="p-5 user-card col-md-4 col-12 ms-auto">
				<Table bordered hover variant="dark">
		            <thead>
		            	<tr className="text-center">
		            		<th colSpan={2} as="h1">{`${firstName} ${lastName}`}</th>
		            	</tr>
		            </thead>
		            <tbody>
		            	<tr>
		            		<td>Mobile Number:</td>
		            		<td className="userValues">{`${mobileNumber}`}</td>
		            	</tr>
		            	<tr>
		            		<td>Admin:</td>
		            		<td className="userValues">{`${isAdmin}`}</td>
		            	</tr>
		            	<tr>
		            		<td>Coupon:</td>
		            		<td className="userValues">{`${coupon}`}</td>
		            	</tr>
		            	<tr>
		            		<td>Email:</td>
		            		<td className="userValues">{`${email}`}</td>
		            	</tr>
		            	<tr>
		            		<td>Password:</td>
		            		<td className="text-center"><Link className="btn btn-primary p-0">Change Password</Link></td>
		            	</tr>
		            	{
			            	(isAdmin) ?
			            	<>
				            	<tr className="text-center">
				            		<td colSpan={2}><Link className="btn btn-primary p-1" onClick={(e) => {
				            			setIsList(!isList)
				            			setAreAllUsers(false)
				            			setIsCreated(false)
				            			setIsAllService(false)
				            			allOrders(e)
				            		}}>All Users Orders</Link></td>
				            	</tr>
				            	<tr className="text-center">
				            		<td colSpan={2}><Link className="btn btn-primary p-1" onClick={() => {
				            			setIsCreated(!isCreated)
				            			setAreAllUsers(false)
				            			setIsList(false)
				            			setIsAllService(false)
				            		}}>Create Service</Link></td>
				            	</tr>
				            	<tr className="text-center">
				            		<td colSpan={2}><Link className="btn btn-primary p-1" onClick={(e) => {
				            			allProducts(e)
				            			setIsAllService(!isAllService)
				            			setAreAllUsers(false)
				            			setIsList(false)
				            			setIsCreated(false)
				            		}}>View All Services</Link></td>
				            	</tr>
				            	<tr className="text-center">
				            		<td colSpan={2}><Link className="btn btn-primary p-1" onClick={(e) => {
				            			allUsers(e)
				            			setAreAllUsers(!areAllUsers)
				            			setIsAllService(false)
				            			setIsList(false)
				            			setIsCreated(false)
				            		}}>View All Users</Link></td>
				            	</tr>
				            	
			            	</>
			            	:
			            	<>
				            	{
				            	(cart) ?
				            	<tr className="text-center">
				            		<td colSpan={2}><Button variant ="secondary" disabled className="-1">My Orders</Button></td>
				            	</tr>				            	:
				            	<tr className="text-center">
				            		<td colSpan={2}><Link className="btn btn-primary p-1" onClick={(e) => {
				            			setIsList(!isList)
				            			setCart(false)
				            			myCart(e)
				            			myOrders(e)
				            		}}>My Orders</Link></td>
				            	</tr>
				            	}
				            	<tr className="text-center">
				            		<td><Link className="btn btn-primary p-1" onClick={(e) => {
				            			setIsList(false)
				            			setCart(!cart)
				            			myOrders(e)
				            			myCart(e)
				            		}}>My Cart</Link></td>
				            		{
				            		(cart) ?
				            		<td><Link className="btn btn-primary p-1" onClick={(e) => {
				            			setCart(false)
				            			setRefresh(true)
				            			myOrders(e)
				            			myCart(e)
				            		}}>Update</Link></td>
				            		:
				            		<td><Button variant="secondary" className="p-1" disabled>Update</Button></td>
				            		}
				            	</tr>
			            	</>
		            	}
		            </tbody>
		        </Table>
			</Container>
			
			{
				(areAllUsers) ?
				<Container className="p-5 order-card col-md-6 col-12">
					<h1 className="text-center" key={1}>All Users</h1>
					{users}
				</Container>
				:
				(isList) ?
				<Container className="p-5 order-card col-md-6 col-12">
					<h1 className="text-center" key={1}>All Orders</h1>
					{orders}
				</Container>
				:
				(isCreated) ?
				<Container className="p-5 order-card col-md-6 col-12">
					<Form className="form-color p-5" onSubmit={(e) => addProduct(e)}>
		    			<h1 className="text-center" key={1}>Add New Service</h1>
						<Form.Group controlId="service-name">
							<Form.Label className="mt-2 mb-0 text-light">Service Name</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter service name"
								value={productName}
								onChange={e=>setProductName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="service-description">
							<Form.Label className="mt-2 mb-0 text-light">Description</Form.Label>
							<Form.Control
								type="textarea"
								placeholder="Enter description"
								value={description}
								onChange={e=>setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="service-price">
							<Form.Label className="mt-2 mb-0 text-light">Price</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter price in Php"
								value={price}
								onChange={e=>setPrice(e.target.value)}
								required
							/>
						</Form.Group>
						{
							(isCreateButton) ?
							<Button variant="primary" type="submit" id="submitBtn" className="mt-2">ADD</Button>
							:
							<Button variant="secondary" type="submit" id="submitBtn" className="mt-2" disabled>ADD</Button>
						}
					</Form>
				</Container>
				:
				(isAllService) ?
				<Container className="p-5 order-card col-md-6 col-12">
					<h1 className="text-center" key={1}>All Products</h1>
					{products}
				</Container>
				:
				(cart) ?
				<Container className="p-5 order-card col-md-6 col-12 text-center">
					<h1 key={1}>Cart Items</h1>
					{thisCart}
					<h2>Php <h2 className="product-total">{total}</h2></h2>
					{
					(thisCart.length <= 0) ?
					<Button className="p-2 mt-3" variant="primary" disabled>CHECKOUT CART</Button>
					:
					<Link className="btn btn-primary p-2 mt-3" to="/checkout">CHECKOUT CART</Link>
					}
				</Container>
				:
				<Container className="p-5 order-card col-md-6 col-12"></Container>
			}
			
		</Row>
    )
}