import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Row, Form } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';


export default function AllUsers({userAll}) {

	const { _id, firstName, lastName, email, mobileNumber, coupon, isAdmin } = userAll;

	const { user } = useContext(UserContext);

	const [id, setId] = useState(_id);
	const [first, setFirst] = useState(firstName);
	const [last, setLast] = useState(lastName);
	const [emailAdd, setEmailAdd] = useState(email);
	const [mobile, setMobile] = useState(mobileNumber);
	const [admin, setAdmin] = useState(isAdmin);

	const [makeAdmin, setMakeAdmin] = useState(isAdmin);

	function adminToggle(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/admin/${_id}`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	},
        	body : JSON.stringify({
        		isAdmin : !admin
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Admin Rights Modified!',
        		  text: 'You have successfully modified admin rights of this user.'
        		})
        		setId(_id)
        		setFirst(first);
        		setLast(last);
        		setEmailAdd(emailAdd);
        		setMobile(mobile);
        		setAdmin(!admin);
        	} else {
        		console.log(data);

        		Swal.fire({
        		  icon: 'error',
        		  title: 'ERROR MODIFYING ADMIN RIGHTS!',
        		  text: 'You are not the Main Admin of the system. Please email the system owner.'
        		})
        	}
        })
	}
	useEffect(()=>{
		setMakeAdmin(user.isAdmin)
		if(user.email==="admin@mail.com" && user.isAdmin===true){
			setMakeAdmin(true)
		} else {
			setMakeAdmin(false)
		}
	}, [user.email, user.isAdmin, makeAdmin])


	return (
		<Container className="p-3">
			<Card className="col-12 col-md-12">
	            <Card.Body>
	                <Card.Title>{`User ID: ${id}`}</Card.Title>
	                <Card.Title as="h2">{`${first} ${last}`}</Card.Title>
	                <Card.Title>{`Email Address:`}</Card.Title>
	                <Card.Text as="h6">{`${emailAdd}`}</Card.Text>
	                <Card.Title>{`Mobile ${mobile}`}</Card.Title>
	                <Card.Subtitle>{`Admin: ${admin}`}</Card.Subtitle>
	                {
	                (makeAdmin) ?
	                <>
	                {
	                admin ?
	                <Link className="btn btn-danger mt-2" onClick={(e) => {
	                	adminToggle(e)
	                	setAdmin(false)
	                }}>Revoke Admin</Link>
	                :
	                <Link className="btn btn-success mt-2" onClick={(e) => {
	                	adminToggle(e)
	                	setAdmin(true)
	                }}>Set Admin</Link>
	            	}
	                </>
	                :
	                <></>
	                }
	            </Card.Body>
	        </Card>
		</Container>
    )
}