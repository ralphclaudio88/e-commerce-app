import {useContext} from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return (

		<Navbar bg="dark" expand="lg" sticky="top" variant="dark">
			<Container fluid>
				<Navbar.Brand as={NavLink} className="mylogo" to="/">RCLAUD</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav change-color"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={NavLink} className="navLink" to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} className="navLink" to="/services">Services</Nav.Link>
						<Nav.Link as={NavLink} className="navLink" to="/about">About</Nav.Link>
						{
							(user.id) ?
							<>
								<Nav.Link as={NavLink} className="navLink" to="/userDashboard">Dashboard</Nav.Link>
								<Nav.Link as={NavLink} className="navLink" to="/logout">Logout</Nav.Link>
							</>
							:
							<>
								<Nav.Link as={NavLink} className="navLink" to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} className="navLink" to="/register">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}