import React from 'react';
import Container from 'react-bootstrap/Container';

export default function Footer() {
	return(

		<Container className="text-center footer-bg">
			<span>&copy; 2023 RClaud. All Rights Reserved.</span>
		</Container>

	)
}