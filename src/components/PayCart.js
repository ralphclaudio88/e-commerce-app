import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Form, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function PayCart({cartItems}) {

	const { _id, totalAmount, isPaid, purchasedOn, owner, isValid } = cartItems;

	const { user } = useContext(UserContext);


	return cartItems.products.map((orderContent, i) => {
		return (
			<Container className="p-3" key={i}>
				<Card className="col-12 order-card text-light">
		            <Card.Body className="bg-secondary">
		                <Card.Title>{`OrderId: ${_id}`}</Card.Title>
		                <Card.Title>{`Order Owner: ${owner}`}</Card.Title>
		                <Card.Title as="h2">{`${orderContent.productName}`}</Card.Title>
		                <Card.Text className="m-0">{`Quantity: ${orderContent.quantity}`}</Card.Text>
		                <Card.Text>{`Unit Price: Php ${orderContent.unitPrice}`}</Card.Text>
		                <Card.Text>{`Total: Php ${totalAmount}`}</Card.Text>
		                <Card.Text>{`Paid: ${isPaid}`}</Card.Text>
		                <Card.Text>{`Validity: ${isValid}`}</Card.Text>
		                <Card.Subtitle>{`PurchasedOn: ${purchasedOn}`}</Card.Subtitle>
		            </Card.Body>
		        </Card>
			</Container>
    	)
	})
}