import { useState, useEffect } from 'react';
import { Card, Button, Container, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ServiceCard({service}) {

	const { _id, productName, description, price } = service;
	return (
			<Container className="p-3 card-prop">
				<Card className="col-12 col-md-6">
		            <Card.Body>
		                <Card.Title>{productName}</Card.Title>
		                <Card.Subtitle>Description:</Card.Subtitle>
		                <Card.Text>{description}</Card.Text>
		                <Card.Subtitle>Price:</Card.Subtitle>
		                <Card.Text>Php {price}</Card.Text>
		                <Link className="btn btn-primary" to={`/services/${_id}`}>Details</Link>
		            </Card.Body>
		        </Card>
			</Container>
    )
}