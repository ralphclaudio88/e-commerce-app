import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Form, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Cart({cartItems}) {

	const { _id, totalAmount, isPaid, purchasedOn, owner, isValid } = cartItems;

	const { user } = useContext(UserContext);

	const [isCancelled, setIsCancelled] = useState(isValid);

	const [newQuantity, setNewQuantity] = useState(true);


	function cancelOrder(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/orders/deleteOrder/${_id}`,{
        	method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then((data,i) => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Order Cancelled!',
        		  text: 'You have cancelled this order. Please click update before paying to apply changes.'
        		})
            	setIsCancelled(false);
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in cancelling!',
        		  text: 'Order is either already cancelled or paid. Please click your cart again before paying.'
        		})
        	}
        })
	}

	return cartItems.products.map((orderContent, i) => {
		const [quantity, setQuantity] = useState(orderContent.quantity);
		const [newTotal, setNewTotal] = useState(totalAmount);


		function changeQuantity(e) {
			e.preventDefault()
			
			fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/changeQuantity`,{
	        	method : "PATCH",
	        	headers : {
	        		"Content-Type" : "application/json",
	        		Authorization : `Bearer ${localStorage.getItem('token')}`
	        	},
	        	body : JSON.stringify({
	        		quantity : quantity
	        	})
	        })
	        .then(res => res.json())
	        .then(data => {
	        	if(data){
	        		Swal.fire({
	        		  icon: 'success',
	        		  title: 'Quantity Changed!',
	        		  text: 'Quantity of order has been modified. Please click your cart again to update the changes.'
	        		})
	        		setNewTotal(quantity*orderContent.unitPrice);
	        	} else {

	        		Swal.fire({
	        		  icon: 'error',
	        		  title: 'Error in Quantity Change!',
	        		  text: 'Quantity was not changed.'
	        		})
	        	}
	        })
		}

		return (
			<Container className="p-3" key={i}>
				<Card className="col-12 order-card text-light">
		            <Card.Body className="bg-secondary">
		                <Card.Title>OrderId: <h5 className="product-values">{_id}</h5></Card.Title>
		                <Card.Title>Order Owner: <h4 className="product-values d-inline">{owner}</h4></Card.Title>
		                <Card.Title as="h2">{`${orderContent.productName}`}</Card.Title>
		                {
		                newQuantity ?
		                <>
		                <Card.Text className="m-0">Quantity: <h5 className="product-values">{quantity}</h5></Card.Text>
		                <Link className="btn btn-primary mb-2" onClick={(e) => {
        					setNewQuantity(!newQuantity)
		                }}>Edit Quantity</Link>
		                </>
		                :
		                <>
		                <Container className="col-sm-2">
		                <Form.Control
		                	type="number"
		                	placeholder="Enter quantity"
							min="1"
		                	value={quantity}
		                	onChange={e=>setQuantity(e.target.value)}
		                	required
		                />
		                </Container>
		                <Link className="btn btn-primary m-2" onClick={(e) => {
        					setNewQuantity(!newQuantity)
		                	changeQuantity(e)
		                }}>Change</Link>
		                <Link className="btn btn-danger m-2" onClick={(e) => {
        					setNewQuantity(!newQuantity)
		                }}>Cancel</Link>
		                </>
		                }
		                
		                <Card.Text>Unit Price: <h5 className="product-values">{`Php ${orderContent.unitPrice}`}</h5></Card.Text>
		                <Card.Text>Total: <h5 className="product-values">{`Php ${newTotal}`}</h5></Card.Text>
		                <Card.Text>Paid: <h5 className="product-bool">{`${isPaid}`}</h5></Card.Text>
		                <Card.Text>Validity: <h5 className="product-bool">{`${isCancelled}`}</h5></Card.Text>
		                <Card.Subtitle>PurchasedOn: <h5 className="product-values">{`${purchasedOn}`}</h5></Card.Subtitle>
		                {
		                user.isAdmin ?
		                <></>
		                :
		                <>
		                <Link className="btn btn-danger mt-2" onClick={(e) => {
		                	cancelOrder(e)
		                }}>Cancel</Link>
		                </>
		            	}
		            </Card.Body>
		        </Card>
			</Container>
    	)
	})
}