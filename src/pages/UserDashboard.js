import { useEffect, useState, useContext } from 'react';
import { Card, Button, Container, Col, Row } from 'react-bootstrap';

import Cart from '../components/Cart.js';
import UserDetails from '../components/UserDetails.js';
import OrderHistory from '../components/OrderHistory.js';
import UserContext from '../UserContext';


export default function UserDashboard() {

	const [details, setDetails] = useState();


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers : {
    			Authorization : `Bearer ${localStorage.getItem('token')}`
    		}
		})
		.then(res => res.json())
		.then(data => {
			setDetails(() => {
				return (
					<UserDetails key={data._id} user={data} />
				)
			})
		})

	}, []);

	return (
		<Row>
			<Col>{details}</Col>
		</Row>
	)
}