import { useEffect, useState } from 'react';
import ServiceCard from '../components/ServiceCard.js';


export default function Services() {

	const [services, setServices] = useState();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
			.then(res => res.json())
			.then(data => {
				setServices(data.map(service => {
					return (
						<ServiceCard key={service._id} service={service} />
					)
				}))
			})
	}, []);

	return (

		<>
			<h1 className="text-center pt-3">SERVICES</h1>
			{services}
		</>
		
	)
}