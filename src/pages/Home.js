import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

    const data = {
        title: "RClaud Services",
        content: "Below summarizes the kinds of services offered.",
        destination: "/services",
        label: "Services Offered"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}