import { useState, useEffect, useContext } from 'react';
import { Card, Button, Container, Col, Row, Table, Form } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import PayCart from '../components/PayCart.js';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CartCheckout() {

	const { user } = useContext(UserContext);

	const [details, setDetails] = useState();
	const [isCheckout, setIsCheckout] = useState(false);
	const [thisCart, setThisCart] = useState()
	let [total, setTotal] = useState(0);

	const navigate = useNavigate();

	function cartCheckout(e) {
		fetch(`${process.env.REACT_APP_API_URL}/orders/cart/checkout`,{
			method : "PATCH",
        	headers : {
        		"Content-Type" : "application/json",
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	},
        	body : JSON.stringify({
        		isPaid : isCheckout
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		setIsCheckout(true)
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Cart Has Been Checkout!',
        		  text: 'You have successfully purchased all items in cart.'
        		})
        		navigate("/userDashboard")
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Error in purchasing items in cart!',
        		  text: 'Please click your cart again before paying.'
        		})
        	}
		})
	}

	function myCart(e) {
		fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,{
        	headers : {
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(cartData => {
        	setThisCart(cartData.map(cartItem => {
				return (
					<PayCart key={cartItem._id} cartItems={cartItem} />
				)
			}))
		})
	}

	function calculateTotal(e){

		let finalTotal = 0;

		fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,{
        	headers : {
        		Authorization : `Bearer ${localStorage.getItem('token')}`
        	}
        })
        .then(res => res.json())
        .then(cartData => {
        	let totalArr = cartData.map(cartItem => {
				return cartItem.totalAmount + total;
			})
			if(totalArr.length !== 0){
				finalTotal = totalArr.reduce((a,b)=>a+b)
				let initializeTotal = finalTotal;
				return setTotal(initializeTotal);
			}
			
		})
	}

	useEffect((e)=>{
		myCart(e);
		if(isCheckout){
			cartCheckout(e);
		}
	}, [isCheckout])

	useEffect((e)=> {
		if(user.isAdmin===false){
			calculateTotal(e);
		};
	}, [])

	return (
		<Container className="text-center">
			<h1 className="p-3">CART CHECKOUT</h1>
			{thisCart}
			<h2>Php <h2 className="product-total">{total}</h2></h2>
			<Link className="btn btn-primary col-md-8 mb-3" onClick={(e) => {
				setIsCheckout(true)
				cartCheckout(e)
			}}>PAY</Link>
		</Container>
	)
}