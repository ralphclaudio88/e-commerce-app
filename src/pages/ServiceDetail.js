import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function ServiceDetail() {

	const {serviceId} = useParams();

	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [price, setPrice] = useState(0);

	const avail = (serviceId) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
				userId : user.id,
				productId : serviceId,
				productName : productName,
				quantity : quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					icon : 'success',
					title : 'Successfully Added to Cart',
					text : "You have successfully added this service offer to cart."
				});
				navigate("/services");
			} else {
				Swal.fire({
					icon : 'error',
					title : 'ERROR ADDING TO CART',
					text : "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/product/${serviceId}`)
			.then(res => res.json())
			.then(data => {
				setProductName(data.productName);
				setDescription(data.description);
				setPrice(data.price);
				console.log(data);
			})

	}, [serviceId]);

	return (
		<Container className="p-5">
			<Row>
				<Col lg={{span : 6, offset : 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Container className="col-sm-4">
								<Form.Control
									className="col-2 mb-3 form-control "
									type="number"
									placeholder="Quantity"
									min="1"
									value={quantity}
									onChange={e=>setQuantity(e.target.value)}
									required
								/>
							</Container>
							{
								(user.id) ?
								<>
								{
								(user.isAdmin) ?
								<Button variant="primary" disabled>Avail</Button>
								:
								<Button variant="primary" onClick={() => avail(serviceId)}>Avail</Button>
								}
								</>
								:
								<Link className="btn btn-danger" to="/login">Log in to avail.</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}