import { useState, useEffect, useContext } from 'react';
import { Button, Col, Container, Form, Image, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
        	method : "POST",
        	headers : {
        		"Content-Type" : "application/json"
        	},
        	body : JSON.stringify({
        		email : email,
        		password : password
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(typeof data.access !== "undefined"){
        		localStorage.setItem('token', data.access);
        		retrieveUserDetails(data.access);

        		Swal.fire({
        		  icon: 'success',
        		  title: 'Login Successful!',
        		  text: 'Welcome'
        		})

        		setEmail('');
        		setPassword('');
        		
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Authentication failed',
        		  text: 'Please check your login credentials and try again.'
        		})
        	}
        })

    }

    const retrieveUserDetails = (token) => {
    	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    		headers : {
    			Authorization : `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {
    		setUser({
    			id : data._id,
    			isAdmin : data.isAdmin
    		})
    	})
    }

	useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
    	(user.id) ?
    		<Navigate to="/services" />
    	:
    	<Row className="login">
    		<Col className="text-center col-12 col-lg-6 p-5">
    			<p className="login-text-1">Every 3 purchase of</p>
    			<p className="login-text-2">services above P1000,</p>
    			<p className="login-text-3">you gain coupon</p>
    			<p className="login-text-4">discount on your</p>
    			<p className="login-text-5">next purchase.</p>
    		</Col>
			<Form className="m-auto col-12 col-lg-4 form-color p-5" onSubmit={(e) => authenticate(e)}>
    			<h1 className="m-auto text-center">LOGIN</h1>
	            <Form.Group controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
	                    type="email" 
	                    placeholder="Enter email"
	                    value={email}
	        			onChange={(e) => setEmail(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            <Form.Group controlId="password">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
	                    type="password" 
	                    placeholder="Password"
	                    value={password}
	        			onChange={(e) => setPassword(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            { 
	            	isActive ? 
		                <Button variant="success" type="submit" id="submitBtn" className="mt-2">
		                    Login
		                </Button>
		                : 
		                <Button variant="secondary" type="submit" id="submitBtn" className="mt-2" disabled>
		                    Login
		                </Button>
	            }
	    	</Form>
    	</Row>
    )
}