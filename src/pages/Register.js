import { useState, useEffect, useContext } from 'react';
import { Button, Col, Container, Form, Image, Row } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ mobileNumber, setMobileNumber ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	function registerUser(e){

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
        	method : "POST",
        	headers : {
        		"Content-Type" : "application/json"
        	},
        	body : JSON.stringify({
        		firstName : firstName,
    		    lastName : lastName,
    		    email : email,
    		    mobileNumber : mobileNumber,
    		    password : password1
        	})
        })
        .then(res => res.json())
        .then(data => {
        	if(data){
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Registration Successful!',
        		  text: 'You may now login.'
        		})
        		setFirstName('');
        		setLastName('');
        		setEmail('');
        		setPassword1('');
        		setPassword2('');
        		setMobileNumber('');

        		navigate("/login")
        	} else {
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Duplicate email found',
        		  text: 'Please provide a different email.'
        		})
        	}
        })

	}

	useEffect(() => {
		if((email !== "" && password1 !== "" && password2 !== "" 
			&& firstName !== "" && lastName !== "" && mobileNumber !== "")
			&& (password1 === password2) && (!isNaN(mobileNumber) && mobileNumber>=0)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNumber]);

	return(
		(user.id) ?
    		<Navigate to="/" />
    	:
    	<Row className="register min-vh-100">
			<Form className="m-auto col-lg-4 form-color p-4" onSubmit={(e) => registerUser(e)}>
    			<h1 className="text-center">SIGN UP</h1>
				<Form.Group controlId="userFirstName">
					<Form.Label className="mt-2 mb-0 text-light">First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter first name"
						value={firstName}
						onChange={e=>setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="userLastName">
					<Form.Label className="mt-2 mb-0 text-light">Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter last name"
						value={lastName}
						onChange={e=>setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label className="mt-2 mb-0 text-light">Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e=>setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-light">We'll never share your email with anyone.</Form.Text>
				</Form.Group>

				<Form.Group controlId="userMobileNumber">
					<Form.Label className="mt-2 mb-0 text-light">Mobile Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter mobile number"
						minLength={11}
						maxLength={11}
						value={mobileNumber}
						onChange={e=>setMobileNumber(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label className="mt-2 mb-0 text-light">Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter a password"
						value={password1}
						onChange={e=>setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label className="mt-2 mb-0 text-light">Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Confirm your password"
						value={password2}
						onChange={e=>setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive ?
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
					:
					<Button variant="secondary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
				}
			</Form>
			
    		<Container className="text-center m-auto col-lg-6 p-5">
    			<p className="sign-up-text-1">Register to get 10%</p>
    			<p className="sign-up-text-2">discount on your first</p>
    			<p className="sign-up-text-3">avail of service offer.</p>
    		</Container >

    	</Row>

	)
}